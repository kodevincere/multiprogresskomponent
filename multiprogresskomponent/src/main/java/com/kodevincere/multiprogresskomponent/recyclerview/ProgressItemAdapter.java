package com.kodevincere.multiprogresskomponent.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.kodevincere.multiprogresskomponent.ProgressItem;
import com.kodevincere.multiprogresskomponent.ProgressItem.OnNewProgressClickedListener;
import com.kodevincere.multiprogresskomponent.ProgressItem.ProgressStatus;
import com.kodevincere.multiprogresskomponent.R;

import java.util.ArrayList;

/**
 * Created by alphonselric on 5/1/16.
 */
public class ProgressItemAdapter extends RecyclerView.Adapter<ProgressItemAdapter.ProgressViewHolder>{

    private ArrayList<ProgressItem> itemList = new ArrayList<>(10);
    private OnNewProgressClickedListener clickListener;

    @Override
    public ProgressViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.progress_item, parent, false);

        return new ProgressViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ProgressViewHolder holder, int position) {
        holder.bind(itemList.get(position));
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void setItemList(ArrayList<ProgressItem> itemList) {
        this.itemList = itemList;
    }

    public ArrayList<ProgressItem> getItemList() {
        return itemList;
    }

    public void addVisibleItem(ProgressItem progressItem) {
        this.itemList.add(progressItem);
        notifyItemInserted(this.itemList.size() - 1);
    }

    public void setClickListener(OnNewProgressClickedListener clickListener) {
        this.clickListener = clickListener;
    }

    public void removeItem(int index) {
        itemList.remove(index);
        notifyItemRemoved(index);
    }

    class ProgressViewHolder extends RecyclerView.ViewHolder{

        private ImageView ivProgressThumbnail, ivProgressStatus;
        private ProgressBar pbProgress;

        public ProgressViewHolder(View itemView) {
            super(itemView);

            ivProgressThumbnail = (ImageView) itemView.findViewById(R.id.iv_progress_thumbnail);
            ivProgressStatus = (ImageView) itemView.findViewById(R.id.iv_progress_status);
            pbProgress = (ProgressBar) itemView.findViewById(R.id.pb_progress);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(clickListener != null) {
                        int adapterPosition = getAdapterPosition();
                        if(adapterPosition > -1)
                            clickListener.onNewProgressClicked(itemList.get(adapterPosition));
                    }
                }
            });
        }

        public void bind(ProgressItem progressItemElement){
            pbProgress.setProgress(progressItemElement.getProgress());
            int progressThumbnail = progressItemElement.getProgressResource();

            ProgressStatus status = progressItemElement.getStatus();
            int statusResourceId = progressItemElement.getProgressFinishedResource();
            int statusVisibility = View.INVISIBLE;
            int thumbnailVisibility = View.INVISIBLE;

            switch (status){
                case UNSTARTED:
                    thumbnailVisibility = View.VISIBLE;
                    break;
                case GOING:
                    thumbnailVisibility = View.VISIBLE;
                    break;
                case FINISHED:
                    statusVisibility = View.VISIBLE;
                    statusResourceId = progressItemElement.getProgressFinishedResource();
                    break;
                case FAILED:
                    statusVisibility = View.VISIBLE;
                    statusResourceId = progressItemElement.getProgressFailedResource();
                    break;
            }

            ivProgressThumbnail.setVisibility(thumbnailVisibility);
            ivProgressStatus.setVisibility(statusVisibility);

            try {
                ivProgressStatus.setImageResource(statusResourceId);
                ivProgressThumbnail.setImageResource(progressThumbnail);
            }catch(Exception ex){}
        }
    }
}