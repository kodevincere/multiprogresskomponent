package com.kodevincere.multiprogresskomponent;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kodevincere.multiprogresskomponent.ProgressItem.ProgressStatus;
import com.kodevincere.multiprogresskomponent.recyclerview.ProgressItemAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alphonselric on 5/1/16.
 */
public class MultiProgressView  extends LinearLayout implements ProgressItem.OnNewProgressClickedListener {

    private static final int DEFAULT_VISIBLE_ITEMS = 2;

    private RecyclerView rvVisibleItems;
    private ProgressItemAdapter visibleItemsAdapter;
    private ArrayList<ProgressItem> visibleItems = new ArrayList<>(20);

    private int maxVisibleItems = DEFAULT_VISIBLE_ITEMS;

    private TextView tvPendingElements;
    private List<ProgressItem> pendingItems = new ArrayList<>(20);
    private List<ProgressItem> finishedItems = new ArrayList<>(20);
    private List<ProgressItem> failedItems = new ArrayList<>(20);
    private OnProgressItemClickListener onProgressItemListener;

    public MultiProgressView(Context context) {
        super(context);
        initializeViews(context);
    }

    public MultiProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeViews(context);
    }

    public MultiProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeViews(context);
    }

    private void initializeViews(Context context){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.multi_progress, this);
        this.setOrientation(VERTICAL);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        initRecycler();

        this.tvPendingElements = (TextView) findViewById(R.id.pending_elements);

        this.tvPendingElements.setVisibility(GONE);
        this.setVisibility(GONE);
    }

    @Override
    public void onNewProgressClicked(ProgressItem progressItem) {
        if(this.onProgressItemListener != null)
            this.onProgressItemListener.onMultiProgressItemClicked(progressItem);
    }

    public void setOnProgressItemListener(OnProgressItemClickListener onProgressItemListener) {
        this.onProgressItemListener = onProgressItemListener;
    }

    public List<ProgressItem> getPendingItems(){
        return this.pendingItems;
    }

    public List<ProgressItem> getFailedItems(){
        return this.failedItems;
    }

    public List<ProgressItem> getFinishedItems(){
        return this.finishedItems;
    }

    public void setMaxVisibleItems(int maxVisibleItems){
        this.maxVisibleItems = maxVisibleItems;
    }

    public void addProgressItem(String id){
        addProgressItem(new ProgressItem(id));
    }

    public void addProgressItem(ProgressItem progressItem){
        this.setVisibility(VISIBLE);

        if(this.visibleItems.size() < maxVisibleItems)
            this.visibleItemsAdapter.addVisibleItem(progressItem);

        this.pendingItems.add(progressItem);

        fixHeight();

        evalPendingElementsVisibility();
    }

    public void removeProgressItem(String id){
        int index = getIndexOfProgressWithId(id);
        if(index > -1) {
            pendingItems.remove(index);
            if(index < maxVisibleItems) {
                if(pendingItems.size() >= maxVisibleItems)
                    visibleItemsAdapter.addVisibleItem(pendingItems.get(maxVisibleItems - 1));
                visibleItemsAdapter.removeItem(index);
            }
        }

        if(pendingItems.size() == 0)
            this.setVisibility(GONE);

        evalPendingElementsVisibility();
    }

    public void updateProgressWithId(String id, int progress){
        int index = getIndexOfProgressWithId(id);
        if(index > -1) {
            pendingItems.get(index).setProgress(progress);
            if(index < maxVisibleItems)
                visibleItemsAdapter.notifyItemChanged(index);
        }
    }

    public void onFinishedProcessWithId(String id){
        modifyStatusOfItemWithId(id, ProgressStatus.FINISHED);
    }

    public void onFailedProcessWithId(String id){
        modifyStatusOfItemWithId(id, ProgressStatus.FAILED);
    }

    protected void initRecycler(){
        this.rvVisibleItems = (RecyclerView) findViewById(R.id.rv_visible_items);
        visibleItemsAdapter = new ProgressItemAdapter();
        visibleItemsAdapter.setItemList(visibleItems);
        visibleItemsAdapter.setClickListener(this);

        rvVisibleItems.setLayoutManager(new LinearLayoutManager(getContext()));
        rvVisibleItems.setAdapter(visibleItemsAdapter);
    }

    protected void modifyStatusOfItemWithId(String id, ProgressStatus status){
        int index = getIndexOfProgressWithId(id);

        if(index > -1) {
            ProgressItem progress = pendingItems.get(index);

            List<ProgressItem> destinationArray = failedItems;

            if(status == ProgressStatus.FINISHED) {
                progress.onProgressFinished();
                destinationArray = finishedItems;
            }else
                progress.onProgressFailed();

            movePendingItemTo(progress, destinationArray);
            if(index < maxVisibleItems)
                visibleItemsAdapter.notifyItemChanged(index);
        }
    }

    protected void movePendingItemTo(ProgressItem progressItem, List<ProgressItem> destination){
        destination.add(progressItem);
    }

    protected void evalPendingElementsVisibility(){
        if(this.pendingItems.size() > maxVisibleItems) {
            int notVisibleItems = pendingItems.size() - maxVisibleItems;
            this.tvPendingElements.setVisibility(VISIBLE);
            this.tvPendingElements.setText(getContext().getResources().getQuantityString(R.plurals.pending_items, notVisibleItems, notVisibleItems));
        }else
            this.tvPendingElements.setVisibility(GONE);
    }

    protected int getIndexOfProgressWithId(String id){
        return pendingItems.indexOf(new ProgressItem(id));
    }

    protected void fixHeight(){
        rvVisibleItems.getLayoutParams().height = RelativeLayout.LayoutParams.WRAP_CONTENT;
    }

    public interface OnProgressItemClickListener{
        void onMultiProgressItemClicked(ProgressItem progressItem);
    }

}