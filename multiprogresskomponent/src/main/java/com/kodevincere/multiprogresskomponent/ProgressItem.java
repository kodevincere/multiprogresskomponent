package com.kodevincere.multiprogresskomponent;

/**
 * Created by alphonselric on 5/1/16.
 */
public class ProgressItem {

    public static int DEFAULT_PROGRESS_RESOURCE = R.drawable.ic_progress;
    public static int DEFAULT_PROGRESS_FINISHED_RESOURCE = R.drawable.ic_finished;
    public static int DEFAULT_PROGRESS_FAILED_RESOURCE = R.drawable.ic_failed;

    private ProgressStatus status = ProgressStatus.UNSTARTED;
    private String id;

    private int progressResource = DEFAULT_PROGRESS_RESOURCE, progressFinishedResource = DEFAULT_PROGRESS_FINISHED_RESOURCE, progressFailedResource = DEFAULT_PROGRESS_FAILED_RESOURCE;
    private int progress;

    public enum ProgressStatus{
        UNSTARTED, GOING, FINISHED, FAILED;
    }

    public ProgressItem(String id){
        this.id = id;
    }

    public ProgressItem(ProgressStatus status, String id){
        this.status = status;
        this.id = id;
    }

    public static void setDefaultProgressResource(int defaultProgressResource){
        DEFAULT_PROGRESS_RESOURCE = defaultProgressResource;
    }

    public static void setDefaultProgressFinishedResource(int defaultProgressFinishedResource){
        DEFAULT_PROGRESS_FINISHED_RESOURCE = defaultProgressFinishedResource;
    }

    public static void setDefaultProgressFailedResource(int defaultProgressFailedResource){
        DEFAULT_PROGRESS_FAILED_RESOURCE = defaultProgressFailedResource;
    }

    public void setId(String id){
        this.id = id;
    }

    public ProgressStatus getStatus(){
        return status;
    }

    public void setProgressResource(int progressResource){
        this.progressResource = progressResource;
    }

    public void setProgressFinishedResource(int progressFinishedResource){
        this.progressFinishedResource = progressFinishedResource;
    }

    public void setProgressFailedResource(int progressFailedResource){
        this.progressFailedResource = progressFailedResource;
    }

    public String getId(){
        return this.id;
    }

    public int getProgressResource() {
        return progressResource;
    }

    public int getProgressFinishedResource() {
        return progressFinishedResource;
    }

    public int getProgressFailedResource() {
        return progressFailedResource;
    }

    public int getProgress() {
        return progress;
    }

    protected void onProgressFinished(){
        status = ProgressStatus.FINISHED;
        this.progress = 100;
    }

    protected void onProgressFailed(){
        status = ProgressStatus.FAILED;
    }

    protected void setProgress(int progress){
        if(progress == 0)
            status = ProgressStatus.UNSTARTED;
        else
            status = ProgressStatus.GOING;

        this.progress = progress;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof ProgressItem)
            return ((ProgressItem)o).getId().equals(this.id);

        return false;
    }

    public interface OnNewProgressClickedListener{
        void onNewProgressClicked(ProgressItem progressItem);
    }
}
