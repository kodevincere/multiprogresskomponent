package com.kodevincere.multiprogress;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.kodevincere.multiprogresskomponent.MultiProgressView;
import com.kodevincere.multiprogresskomponent.ProgressItem;

public class MainActivity extends AppCompatActivity implements MultiProgressView.OnProgressItemClickListener{

    private MultiProgressView multiProgressView;
    private int nextProgressItem = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        multiProgressView = (MultiProgressView) findViewById(R.id.multi_progress_view);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiProgressView.addProgressItem("MultiProgressItem ".concat(String.valueOf(nextProgressItem)));
                nextProgressItem++;
            }
        });

        multiProgressView = (MultiProgressView) findViewById(R.id.multi_progress_view);
        multiProgressView.setMaxVisibleItems(5);

        ProgressItem progress = new ProgressItem("MultiProgressItem 1");
        progress.setProgressFinishedResource(R.drawable.ic_check_circle_black_48dp);
        progress.setProgressFailedResource(R.drawable.ic_cancel_black_48dp);
        progress.setProgressResource(R.drawable.ic_hourglass_empty_black_48dp);

        multiProgressView.addProgressItem(progress);
        multiProgressView.addProgressItem("MultiProgressItem 2");
        multiProgressView.addProgressItem("MultiProgressItem 3");

        multiProgressView.setOnProgressItemListener(this);

        startProgressAnimation();
    }

    private void startProgressAnimation(){
        new CountDownTimer(3000, 500) {
            @Override
            public void onTick(long millisUntilFinished) {
                int left = (int)(3000-millisUntilFinished)/30;
                multiProgressView.updateProgressWithId("MultiProgressItem 1", left);
                multiProgressView.updateProgressWithId("MultiProgressItem 3", left/2);
                multiProgressView.updateProgressWithId("MultiProgressItem 5", left/3);
            }

            @Override
            public void onFinish() {
                multiProgressView.onFailedProcessWithId("MultiProgressItem 1");
                multiProgressView.onFinishedProcessWithId("MultiProgressItem 2");
                multiProgressView.onFinishedProcessWithId("MultiProgressItem 6");
                multiProgressView.onFinishedProcessWithId("MultiProgressItem 15");
            }
        }.start();
    }

    @Override
    public void onMultiProgressItemClicked(ProgressItem progressItem) {
        multiProgressView.removeProgressItem(progressItem.getId());
    }

}
