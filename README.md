# MultiProgressKomponent

MultiProgressKomponent is a simple view that can show multiple items processing in real time and lets you handle them independently.

For example, you have two process running in background, one uploading a picture to your server and another one downloading a video, and you want the user to know easily in your app the actual status of both processes. MultiProgressKomponent is made for you!



![Screenshot](screenshot.png)



Features:

- Add as many progress items as you need, the list will automatically resize for you
- Update any progress individually
- Receive any click event on the items individually
- You define the progress items status, being unstarted, going, finished or failed and do actions depending on that status
- Use default icons, set icons for all the items globally or set icons individually for each progress item!
- You choose how many items will be visible in your list. Remaining items are shown in text
- Open source

And many more!



This project is a simple example about how to use MultiProgressKomponent module. Here you can add as many progress items as you want and remove them just by clicking in them. For this example only some elements will be showing progress.



## How to make it work?

You must include MultiProgressKomponent in your project and add MultiProgressView as a normal view.

For example

```xml
<com.kodevincere.multiprogresskomponent.MultiProgressView
        android:id="@+id/multi_progress_view"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"/>
```



Get your MultiProgressView element like:

```java
MultiProgressView multiProgressView = (MultiProgressView) findViewById(R.id.multi_progress_view);
```



After that, you need to create the progress items like with a String id like:

`ProgressItem progress = new ProgressItem("MultiProgressItem 1");`

And add it to your MultiProgressView

```java
multiProgressView.addProgressItem(progress);
```



If you want to receive click events you must implement `MultiProgressView.OnProgressItemClickListener` interface and the required information will received through the method  `void onMultiProgressItemClicked(ProgressItem progressItem);`

Any update or removal of the ProgressItem object must be done using MultiProgressView methods, tipically receiving only the ProgressItem id.

### Available configuration:

#### From ProgressItem

`ProgressItem.setDefaultProgressResource(int defaultProgressResource);`

Description: Allows you to define DEFAULT progress resource.
Allowed Values: Any drawable or mipmap from your app.
Default Value: `DEFAULT_PROGRESS_RESOURCE`

`ProgressItem.setDefaultProgressFinishedResource(int defaultProgressFinishedResource);`

Description: Allows you to define DEFAULT progress finished resource.
Allowed Values: Any drawable or mipmap from your app.
Default Value: `DEFAULT_PROGRESS_FINISHED_RESOURCE`

`ProgressItemset.DefaultProgressFailedResource(int defaultProgressFailedResource);`

Description: Allows you to define DEFAULT progress failed resource.
Allowed Values: Any drawable or mipmap from your app.
Default Value: `DEFAULT_PROGRESS_FAILED_RESOURCE`

`setProgressResource(int progressResource)`

Description: Allows you to define progress resource FOR THIS ProgressItem.
Allowed Values: Any drawable or mipmap from your app.
Default Value: `DEFAULT_PROGRESS_RESOURCE`
`setProgressFinishedResource(int progressFinishedResource)`

Description: Allows you to define progress finished resource FOR THIS ProgressItem.
Allowed Values: Any drawable or mipmap from your app.
Default Value: `DEFAULT_PROGRESS_FINISHED_RESOURCE`
`setProgressFailedResource(int progressFailedResource)`

Description: Allows you to define progress failed resource FOR THIS ProgressItem.
Allowed Values: Any drawable or mipmap from your app.
Default Value: `DEFAULT_PROGRESS_FAILED_RESOURCE`
#### From MultiProgressView

`setMaxVisibleItems(int maxVisibleItems);`

Description: Allows you to define max quantity of visible items before showing pending elements text.
Allowed Values: Any number.
Default Value: 2

`setOnProgressItemListener(OnProgressItemClickListener onProgressItemListener);`

Description: Allows you to define the ProgressItem click listener. All will have the same initially defined listener

`addProgressItem(String id);`

Description: Allows you to add a new ProgressItem objectwith the given id.

`addProgressItem(ProgressItem progressItem);`

Description: Allows you to add a previously created ProgressItem object.

`removeProgressItem(String id);`

Description: Removes element with the given id from pendingItems list.

`getPendingItems();`

Description: Returns all the items added to the view but not removed even if not visible.

`getFinishedItems();`

Description: Returns all the finished items (When `onFinishedProcessWithId(String id)`called).

`getFailedItems();`

Description: Returns all the failed items (When `onFailedProcessWithId(String id)`called).

`onFinishedProcessWithId(String id);`

Description: Tells the view that ProgressItem with given id was successfully finished and is added to finishedItems list.

`onFailedProcessWithId(String id);`

Description: Tells the view that ProgressItem with given id has failed and is added to failedItems list.

## License

```
Copyright 2016 KodeVincere

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```







